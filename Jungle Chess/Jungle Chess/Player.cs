﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jungle_Chess {
    public class Player : ICloneable<Player>{
        public string nama, owner;
        public bool giliran;
        public List<Pion> pions;
        public int SBE;

        public Player Clone() {
            return new Player(this.nama, this.giliran);
        }

        public Player(string nama, bool giliran) {
            this.nama = nama;
            this.giliran = giliran;
            this.pions = new List<Pion>();
            
            //BUAT PION, TRAP DAN DEN
            if (nama == "p1") {
                this.pions.Add(new Pion("LION", "lion1.png", "p1", 0, 8));
                this.pions.Add(new Pion("TIGER", "tiger1.png", "p1", 6, 8));
                this.pions.Add(new Pion("DOG", "dog1.png", "p1", 5, 7));
                this.pions.Add(new Pion("CAT", "cat1.png", "p1", 1, 7));
                this.pions.Add(new Pion("RAT", "rat1.png", "p1", 6, 6));
                this.pions.Add(new Pion("JAGUAR", "jaguar1.png", "p1", 4, 6));
                this.pions.Add(new Pion("WOLF", "wolf1.png", "p1", 2, 6));
                this.pions.Add(new Pion("ELEPHANT", "elephant1.png", "p1", 0, 6));
            } else if (nama == "p2") {
                this.pions.Add(new Pion("LION", "lion2.png", "p2", 0, 0));
                this.pions.Add(new Pion("TIGER", "tiger2.png", "p2", 6, 0));
                this.pions.Add(new Pion("DOG", "dog2.png", "p2", 1, 1));
                this.pions.Add(new Pion("CAT", "cat2.png", "p2", 5, 1));
                this.pions.Add(new Pion("RAT", "rat2.png", "p2", 0, 2));
                this.pions.Add(new Pion("JAGUAR", "jaguar2.png", "p2", 2, 2));
                this.pions.Add(new Pion("WOLF", "wolf2.png", "p2", 4, 2));
                this.pions.Add(new Pion("ELEPHANT", "elephant2.png", "p2", 6, 2));
            }
        }
    }
}
