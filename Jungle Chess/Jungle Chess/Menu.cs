﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jungle_Chess {
    public partial class Menu : Form {
        bool p1, p2;

        public Menu() {
            InitializeComponent();
            this.MaximizeBox = false;
        }

        private void btnPlayer_Click(object sender, EventArgs e) {
            p1 = true;
            p2 = false;

            new Game(p1, p2).ShowDialog();
            this.Hide();
            this.Close();
        }

        private void btnAI_Click(object sender, EventArgs e) {
            p1 = false;
            p2 = true;

            new Game(p1, p2).ShowDialog();
            this.Hide();
            this.Close();
        }
    }
}
