﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jungle_Chess {
    public class Pion : ICloneable<Pion>{
        public string nama, imageName, owner;
        public Image img; 
        //POS X, Y DAN WEIGTH (VALUE)
        public int x, y, weight, value;

        //STEP LANGKAH
        public int dx, dy;

        //BISA LOMPAT
        public bool canJump;

        //BOLEH KE AIR
        public bool walkInWater;

        //MASUK TRAP
        public bool trapped;

        public Pion Clone() {
            return new Pion(this.nama, this.imageName, this.owner, this.x, this.y);
        }

        public Pion(string nama, string imageName, string owner, int x, int y) {
            this.nama = nama;
            this.imageName = imageName;
            this.owner = owner;
            this.img = Image.FromFile("../../img/" + imageName);
            this.x = x; this.y = y;
            this.dx = 1; this.dy = 1;
            this.canJump = false;
            this.walkInWater = false;
            this.trapped = false;
            
            /*
             1 => RAT
             2 => CAT
             3 => DOG
             4 => WOLF
             5 => JAGUAR / LEOPARD
             6 => TIGER
             7 => LION
             8 => ELEPHANT*/

            if (nama == "RAT") {
                this.weight = 1;
                this.value = 600;
                this.walkInWater = true;
            } else if (nama == "CAT") {
                this.weight = 2;
                this.value = 200;
            } else if (nama == "DOG") {
                this.weight = 3;
                this.value = 300;
            } else if (nama == "WOLF") {
                this.weight = 4;
                this.value = 400;
            } else if (nama == "JAGUAR") {
                this.weight = 5;
                this.value = 500;
            } else if (nama == "TIGER") {
                this.weight = 6;
                this.value = 800;
                this.canJump = true;
            } else if (nama == "LION") {
                this.weight = 7;
                this.value= 900;
                this.canJump = true;
            } else if (nama == "ELEPHANT") {
                this.weight = 8;
                this.value = 1000;
            }
        } 

        public bool validMove(int x1, int y1, int x2, int y2, Petak[,] papan) {
            //sekarang = giliran player sekarang
            if (x2 < 0 || x2 > 6 || y2 < 0 || y2 > 8) { //CEK OUT OF BOND
                return false;
            } else {
                //KHUSUS UTK LION/TIGER
                if (this.canJump) {
                    for (int y = 3; y <= 5; y++) { //DARI KIRI/KANAN SUNGAI
                        if (y1 == y) {
                            if (x2 - 3 == x1 || x2 + 3 == x1) {
                                this.dx = 3; this.dy = 4;
                                break;
                            } else {
                                this.dx = 1; this.dy = 1;
                                break;
                            }
                        }
                    }

                    for (int x = 1; x <= 2; x++) { //DARI ATAS/BAWAH SUNGAI
                        if (x1 == x) {
                            if (y2 - 4 == y1 || y2 + 4 == y1) {
                                this.dx = 3; this.dy = 4;
                                break;
                            } else {
                                this.dx = 1; this.dy = 1;
                                break;
                            }
                        }
                    }

                    for (int x = 4; x <= 5; x++) { //DARI ATAS/BAWAH SUNGAI
                        if (x1 == x) {
                            if (y2 - 4 == y1 || y2 + 4 == y1) {
                                this.dx = 3; this.dy = 4;
                                break;
                            } else {
                                this.dx = 1; this.dy = 1;
                                break;
                            }
                        }
                    }
                }

                bool jalanKananKiri = (x1 + this.dx == x2 || x1 - this.dx == x2) && y1 == y2;
                bool jalanAtasBawah = x1 == x2 && (y1 + this.dy == y2 || y1 - this.dy == y2);

                if (jalanKananKiri || jalanAtasBawah) { //CEK BISA BENAR ATAU TIDAK STEPNYA
                    Petak petakLama = papan[x1, y1], petakBaru = papan[x2, y2];

                    if (petakBaru.type == -1) { //RIVER
                        //BUKAN RAT
                        if (!this.walkInWater) return false;
                    } else { //LAHAN KOSONG
                        if (petakBaru.pion != null) { //ADA PION
                            //PION KU
                            if (petakBaru.pion.owner == this.owner) return false;
                            else { //PION MUSUH
                                Pion musuh = petakBaru.pion;

                                if (musuh.weight > this.weight) {
                                    if (musuh.trapped) { //JIKA MUSUH KENA TRAP, BISA MAKAN
                                    } else {
                                        if (this.weight == 1 && musuh.weight == 8) { //KONDISI KHUSUS
                                            //JIKA TIKUSKU DI AIR
                                            if (petakLama.type == -1) return false;
                                        } else return false;
                                    }
                                } else {
                                    //KONDISI KHUSUS
                                    if (this.weight == 8 && musuh.weight == 1) return false;
                                }
                            }
                        } else {
                            if (petakBaru.owner == this.owner && petakBaru.type == -3) return false;    
                        }
                    }
                } else return false;
                return true;
            }   
        }

        public void move(int x2, int y2, Petak[,] papan, Player sekarang, Player lain, bool simulasi = false) {
            
            if (papan[x2, y2].pion == null) { //GA ADA PION
                if (papan[x2, y2].owner != this.owner && papan[x2, y2].owner != "") { //BERADA DI PETAK TRAP /DEN MUSUH 
                    if (papan[x2, y2].type == -3) { //BERADA DI PETAK DEN MUSUH
                        Game.gameEnd = true;
                        this.updatePapan(papan[this.x, this.y], papan[x2, y2], simulasi);
                        this.x = x2; this.y = y2;
                        //System.Windows.Forms.MessageBox.Show("MASUK DEN " + papan[x2, y2].owner);
                    } else if (papan[x2, y2].type == -2) { //BERADA DI PETAK TRAP MUSUH
                        this.trapped = true;
                        this.updatePapan(papan[this.x, this.y], papan[x2, y2], simulasi);
                        this.x = x2; this.y = y2;
                        //System.Windows.Forms.MessageBox.Show("MASUK TRAP " + papan[x2, y2].owner);
                    } 
                } else { //BERADA DI LUAR PETAK TRAP MUSUH/LAHAN LAIN
                    this.trapped = false;
                    this.updatePapan(papan[this.x, this.y], papan[x2, y2], simulasi);
                    this.x = x2; this.y = y2;
                }
            } else { //ADA PION
                if (papan[x2, y2].pion.owner != this.owner) { //PION MUSUH
                    this.eat(x2, y2, papan[x2, y2].pion, papan, sekarang, lain, simulasi);
                }
            }
        }

        public void eat(int x2, int y2, Pion musuh, Petak[,] papan, Player sekarang, Player lain, bool simulasi = false) {
            //System.Windows.Forms.MessageBox.Show(this.nama + "\n" + musuh.nama);

            if (musuh.trapped) {
                lain.pions.Remove(musuh);
                this.updatePapan(papan[this.x, this.y], papan[x2, y2], simulasi);
                this.x = x2; this.y = y2;
                //System.Windows.Forms.MessageBox.Show(lain.nama + " PION MATI, KARENA MASUK DALAM TRAP!");
            } else {
                if (musuh.weight == 8 && this.weight == 1) { //CEK TIKUS KU MAKAN GAJAH
                    lain.pions.Remove(musuh);
                    this.updatePapan(papan[this.x, this.y], papan[x2, y2], simulasi);
                    this.x = x2; this.y = y2;
                    //System.Windows.Forms.MessageBox.Show(lain.nama + " PION MATI, KARENA KONDISI KHUSUS!");
                } else {
                    if (this.weight >= musuh.weight) {
                        if (this.weight == 8 && musuh.weight == 1) {
                        } else {
                            lain.pions.Remove(musuh);
                            this.updatePapan(papan[this.x, this.y], papan[x2, y2], simulasi);
                            this.x = x2; this.y = y2;
                            //System.Windows.Forms.MessageBox.Show(lain.nama + " PION MATI!");
                        }
                    }
                }
            }
        }

        public void updatePapan(Petak petakLama, Petak petakBaru, bool simulasi = false) {
            petakLama.pion = null; //PION PETAK LAMA DIHILANGIN
            petakBaru.pion = this; //PION PETAK BARU DIISI PION SEKARANG

            if (!simulasi) { //JIKA BUKAN SIMULASI - RECURSIVE
                petakLama.setImage(); //PETAK LAMA BG IMAGE DI BALIKIN AWAL
                petakBaru.setImage(this.img); //PETAK BARU DIISI BG IMAGE PION YG DI PINDAH
            }
        }
    }
}