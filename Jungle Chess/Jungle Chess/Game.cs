﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Jungle_Chess {
    public partial class Game : Form {
        public static bool gameEnd;
        private int ALPHA, BETA;
        private Player p1, p2;
        private Petak[,] papan;
        private int simpanHewan, simpanArah;
        private int[] arahGerakX = { 0, 1, 0, -1 };
        private int[] arahGerakY = { -1, 0, 1, 0 };
        private Dictionary<string, int[,]> valuePapan;
        private Stopwatch stopwatch;

        public Game(bool giliran1, bool giliran2) {
            InitializeComponent();
            this.MaximizeBox = false;

            gameEnd = false;
            p1 = new Player("p1", giliran1); 
            p2 = new Player("p2", giliran2);
            papan = new Petak[7, 9];
            valuePapan = new Dictionary<string, int[,]>();
            stopwatch = new Stopwatch();
            createMap(); //BUAT PAPAN
            setupPion(); //TARUH PION, TRAP DAN PLAYER
            createValuePapan(); //BUAT VALUE PAPAN PION

            if (p2.giliran) {
                Console.WriteLine("START!");
                stopwatch.Start();
                gerakAI();
                p1.giliran = true;
                stopwatch.Stop();
                Console.WriteLine("TIME:" + stopwatch.Elapsed);

            }
        }

        private void createMap() {
            for (int y = 0; y < 9; y++) {
                for (int x = 0; x < 7; x++) {
                    papan[x, y] = new Petak(x, y, 70);
                    papan[x, y].setImage();
                    papan[x, y].body.Click += Gerak_Pion;
                    this.Controls.Add(papan[x, y].body);
                }
            }

            for (int y = 3; y <= 5; y++) {
                papan[1, y].type = -1;
                papan[1, y].setImage();
            }

            for (int y = 3; y <= 5; y++) {
                papan[2, y].type = -1;
                papan[2, y].setImage();
            }

            for (int y = 3; y <= 5; y++) {
                papan[4, y].type = -1;
                papan[4, y].setImage();
            }

            for (int y = 3; y <= 5; y++) {
                papan[5, y].type = -1;
                papan[5, y].setImage();
            }

            //DEN + TRAP P2
            papan[2, 0].owner = "p2";
            papan[2, 0].type = -2;
            papan[2, 0].setImage();

            papan[4, 0].owner = "p2";
            papan[4, 0].type = -2;
            papan[4, 0].setImage();

            papan[3, 1].owner = "p2";
            papan[3, 1].type = -2;
            papan[3, 1].setImage();

            papan[3, 0].owner = "p2";
            papan[3, 0].type = -3;
            papan[3, 0].setImage();

            //DEN + TRAP P1
            papan[3, 7].owner = "p1";
            papan[3, 7].type = -2;
            papan[3, 7].setImage();

            papan[2, 8].owner = "p1";
            papan[2, 8].type = -2;
            papan[2, 8].setImage();

            papan[4, 8].owner = "p1";
            papan[4, 8].type = -2;
            papan[4, 8].setImage();

            papan[3, 8].owner = "p1";
            papan[3, 8].type = -3;
            papan[3, 8].setImage();
        }

        private void setupPion() {
            foreach (Pion i in p1.pions) {
                papan[i.x, i.y].pion = i;
                papan[i.x, i.y].setImage(i.img);
            }

            foreach (Pion i in p2.pions) {
                papan[i.x, i.y].pion = i;
                papan[i.x, i.y].setImage(i.img);
            }
        }

        private void createValuePapan() {
            valuePapan.Add("RAT", new int[,] {
                { 8, 8, 8, 0, 8, 8, 8},
                { 8, 8, 8, 9, 9, 9, 9},
                { 8, 8, 8, 9, 10, 10, 10},
                { 10, 12, 9, 10, 12, 12, 11},
                { 11, 12, 9, 11, 12, 12, 12},
                { 12, 12, 9, 11, 12, 12, 13},
                { 10, 11, 11, 13, 13, 13, 13},
                { 11, 12, 13, 50, 13, 13, 13},
                { 11, 13, 50, 9999, 50, 13, 13},
            });

            valuePapan.Add("CAT", new int[,] {
                { 8, 8, 8, 0, 8, 8, 8},
                { 13, 10, 8, 8, 8, 8, 8},
                { 10, 10, 10, 8, 0, 0, 8},
                { 10, 0, 0, 8, 0, 0, 8},
                { 10, 0, 0, 8, 0, 0, 8},
                { 10, 0, 0, 10, 0, 0, 8},
                { 10, 11, 11, 15, 11, 11, 10},
                { 11, 11, 15, 50, 15, 11, 11},
                { 11, 15, 50, 9999, 50, 15, 11},
            });

            valuePapan.Add("DOG", new int[,] {
                { 8, 8, 8, 0, 12, 12, 8},
                { 8, 8, 8, 8, 13, 10, 8},
                { 8, 8, 8, 8, 8, 8, 8},
                { 8, 0, 0, 8, 0, 0, 8},
                { 8, 0, 0, 8, 0, 0, 8},
                { 9, 0, 0, 10, 0, 0, 9},
                { 9, 10, 11, 15, 11, 10, 9},
                { 10, 11, 15, 50, 15, 11, 10},
                { 11, 15, 50, 9999, 50, 15, 11},
            });

            valuePapan.Add("WOLF", new int[,] {
                { 8, 12, 12, 0, 8, 8, 8},
                { 8, 12, 13, 8, 9, 8, 8},
                { 8, 8, 10, 8, 8, 8, 8},
                { 8, 0, 0, 8, 0, 0, 8},
                { 8, 0, 0, 8, 0, 0, 8},
                { 9, 0, 0, 10, 0, 0, 9},
                { 9, 10, 11, 15, 11, 10, 9},
                { 10, 11, 15, 50, 15, 11, 10},
                { 11, 15, 50, 9999, 50, 15, 11},
            });

            valuePapan.Add("JAGUAR", new int[,] {
                { 9, 9, 9, 0, 9, 9, 9},
                { 9, 9, 9, 9, 9, 9, 9},
                { 9, 9, 9, 10, 10, 9, 9},
                { 10, 0, 0, 13, 0, 0, 10},
                { 11, 0, 0, 14, 0, 0, 11},
                { 12, 0, 0, 15, 0, 0, 12},
                { 13, 13, 14, 15, 14, 13, 13},
                { 13, 14, 15, 50, 15, 14, 13},
                { 14, 15, 50, 9999, 50, 15, 14},
            });

            valuePapan.Add("TIGER", new int[,] {
                { 10, 12, 12, 0, 12, 12, 10},
                { 12, 14, 12, 12, 12, 12, 12},
                { 14, 20, 16, 14, 16, 20, 14},
                { 15, 0, 0, 15, 0, 0, 15},
                { 15, 0, 0, 15, 0, 0, 15},
                { 15, 0, 0, 15, 0, 0, 15},
                { 18, 20, 20, 30, 20, 20, 18},
                { 25, 25, 30, 50, 30, 25, 25},
                { 20, 30, 50, 9999, 50, 30, 25},
            });

            valuePapan.Add("LION", new int[,] {
                { 10, 12, 12, 0, 12, 12, 10},
                { 12, 12, 12, 12, 12, 14, 14},
                { 14, 20, 16, 14, 16, 20, 15},
                { 15, 0, 0, 15, 0, 0, 15},
                { 15, 0, 0, 15, 0, 0, 15},
                { 15, 0, 0, 15, 0, 0, 15},
                { 18, 20, 20, 30, 20, 20, 18},
                { 25, 25, 30, 50, 30, 25, 25},
                { 20, 30, 50, 9999, 50, 30, 25},
            });

            valuePapan.Add("ELEPHANT", new int[,] {
                { 11, 11, 11, 0, 11, 11, 11},
                { 14, 11, 11, 14, 13, 12, 11},
                { 15, 15, 14, 14, 15, 14, 12},
                { 12, 0, 0, 12, 0, 0, 12},
                { 14, 0, 0, 14, 0, 0, 14},
                { 16, 0, 0, 16, 0, 0, 16},
                { 18, 20, 20, 30, 20, 20, 18},
                { 25, 25, 30, 50, 30, 25, 25},
                { 25, 30, 50, 9999, 50, 30, 25},
            });
        }

        int x1 = 0, y1 = 0;
        int x2 = 0, y2 = 0;
        int ctrClick = 0;
        Pion clicked = null;

        private void Gerak_Pion(object sender, EventArgs e) {
            String[] split = ((Button)sender).Name.Split('-');
            
            if (p1.giliran) {
                if (ctrClick == 0) { //AMBIL PION
                    x1 = int.Parse(split[0]);
                    y1 = int.Parse(split[1]);

                    //CEK YANG DI CLICK BENAR PIONNYA P1
                    if (papan[x1, y1].pion != null && papan[x1, y1].pion.owner == "p1") {
                        clicked = papan[x1, y1].pion;
                        ctrClick++;
                    }
                } else if (ctrClick == 1) { //TARUH PION
                    x2 = int.Parse(split[0]);
                    y2 = int.Parse(split[1]);

                    if (clicked.validMove(x1, y1, x2, y2, papan)) {
                        clicked.move(x2, y2, papan, p1, p2);
                        resetAction(false, true);
                        //MessageBox.Show("Giliran P2");

                        Console.WriteLine("START!");
                        stopwatch.Start();
                        gerakAI();
                        stopwatch.Stop();
                        Console.WriteLine("TIME:" + stopwatch.Elapsed);

                        if (gameEnd) MessageBox.Show("GAME OVER");
                    } else resetAction(true, false);
                }
            } else if (p2.giliran) {
                gerakHuman(split); //UNTUK TESTING GAME
            }
        }

        private void gerakHuman(String[] split) {
            if (ctrClick == 0) { //AMBIL PION
                x1 = int.Parse(split[0]);
                y1 = int.Parse(split[1]);

                //CEK YANG DI CLICK BENAR PIONNYA P2
                if (papan[x1, y1].pion != null && papan[x1, y1].pion.owner == "p2") {
                    clicked = papan[x1, y1].pion;
                    ctrClick++;
                }
            } else if (ctrClick == 1) { //TARUH PION
                x2 = int.Parse(split[0]);
                y2 = int.Parse(split[1]);

                if (clicked.validMove(x1, y1, x2, y2, papan)) {
                    clicked.move(x2, y2, papan, p2, p1);
                    resetAction(true, false);
                    //MessageBox.Show("Giliran P1");
                } else resetAction(false, true);
            }
        }

        private void gerakAI() {
            ALPHA = -99999; BETA = 99999;
            minimax(0, papan, p1, p2, ALPHA, BETA);

            int destX = p2.pions[simpanHewan].x + arahGerakX[simpanArah];
            int destY = p2.pions[simpanHewan].y + arahGerakY[simpanArah];

            //MessageBox.Show(p2.pions[simpanHewan].nama+"\nX: "+destX+"\n"+ "Y: " + destY);

            p2.pions[simpanHewan].move(destX, destY, papan, p2, p1);
            //MessageBox.Show("Giliran P1");
        }

        private void resetAction(bool p1Turn, bool p2Turn) {
            x1 = 0; y1 = 0;
            x2 = 0; y2 = 0;
            ctrClick = 0;
            clicked = null;
            //p1.giliran = p1Turn;
            //p2.giliran = p2Turn;
        }
        
        private int minimax(int depth, Petak[,] papan, Player p1, Player p2, int ALPHA, int BETA) {      
            if (depth == 2) {
                int SBE = 0;

                for (int y= 0; y<9; y++) {
                    for (int x = 0; x < 7; x++) {
                        Pion pion = papan[x, y].pion;

                        if (papan[x, y].owner == "" && pion != null) {
                            if (pion.owner == "p2") SBE += pion.value + valuePapan[pion.nama][pion.y, pion.x];
                            else SBE -= pion.value + valuePapan[pion.nama][pion.y, pion.x];
                        }
                    }
                }
                 
                return SBE;
            } else {
                if (depth % 2 == 0) { //PREDIKSI GERAK AI
                    int batas = -99999;
                    
                    for (int i = p2.pions.Count - 1; i >= 0; i--) {
                        for (int arah = 0; arah < 4; arah++) {
                            //CLONE PLAYER, AI 
                            Player player = p1.Clone();
                            player.pions = new List<Pion>();
                            player.pions = p1.pions.ConvertAll(p => p.Clone());

                            Player AI = p2.Clone();
                            AI.pions = new List<Pion>();
                            AI.pions = p2.pions.ConvertAll(p => p.Clone());

                            //CLONE PAPAN
                            Petak[,] dpapan = new Petak[7, 9];
                            for (int y = 0; y < 9; y++) {
                                for (int x = 0; x < 7; x++) {
                                    dpapan[x, y] = new Petak(papan[x, y]);
                                    dpapan[x, y].pion = null;
                                }
                            }

                            //ISI PAPAN LAGI BIAR FRESH
                            for (int p = 0; p < p1.pions.Count; p++) {
                                dpapan[p1.pions[p].x, p1.pions[p].y].pion = p1.pions[p];
                            }

                            for (int p = 0; p < p2.pions.Count; p++) {
                                dpapan[p2.pions[p].x, p2.pions[p].y].pion = p2.pions[p];
                            }

                            Pion pion = AI.pions[i];
                            int x1 = pion.x, y1 = pion.y;
                            int x2 = x1 + arahGerakX[arah], y2 = y1 + arahGerakY[arah];

                            if (pion.validMove(x1, y1, x2, y2, papan)) {
                                pion.move(x2, y2, dpapan, AI, player, true);

                                //HITUNG SBE
                                int temp = minimax(depth + 1, dpapan, player, AI, ALPHA, BETA);

                                if (batas < temp) {
                                    if (depth == 0) {
                                        simpanHewan = i;
                                        simpanArah = arah;
                                    }

                                    batas = temp;
                                }

                                //PRUNNING
                                if (ALPHA < batas) ALPHA = batas;
                                if (BETA <= ALPHA) break;
                            }
                        }
                    }

                    return batas;
                } else { //PREDIKSI GERAK PLAYER
                    int batas = 99999;

                    for (int i = p1.pions.Count - 1; i >= 0; i--) {
                        for (int arah = 0; arah < 4; arah++) {
                            //CLONE PLAYER, AI
                            Player player = p1.Clone();
                            player.pions = new List<Pion>();
                            player.pions = p1.pions.ConvertAll(p => p.Clone());

                            Player AI = p2.Clone();
                            AI.pions = new List<Pion>();
                            AI.pions = p2.pions.ConvertAll(p => p.Clone());
                                
                            //CLONE PAPAN
                            Petak[,] dpapan = new Petak[7, 9];
                            for (int y = 0; y < 9; y++) {
                                for (int x = 0; x < 7; x++) {
                                    dpapan[x, y] = new Petak(papan[x, y]);
                                    dpapan[x, y].pion = null;
                                }
                            }

                            //ISI PAPAN LAGI BIAR FRESH
                            for (int p = 0; p < p1.pions.Count; p++) {
                                dpapan[p1.pions[p].x, p1.pions[p].y].pion = p1.pions[p];
                            }

                            for (int p = 0; p < p2.pions.Count; p++) {
                                dpapan[p2.pions[p].x, p2.pions[p].y].pion = p2.pions[p];
                            }

                            Pion pion = player.pions[i];
                            int x1 = pion.x, y1 = pion.y;
                            int x2 = x1 + arahGerakX[arah], y2 = y1 + arahGerakY[arah];
                            
                            if (pion.validMove(x1, y1, x2, y2, papan)) {
                                pion.move(x2, y2, dpapan, player, AI, true);

                                //HITUNG SBE
                                int temp = minimax(depth + 1, dpapan, player, AI, ALPHA, BETA);

                                if (batas > temp) {
                                    if (depth == 0) batas = temp;

                                    batas = temp;
                                }

                                //PRUNNING
                                if (ALPHA > batas) BETA = batas;
                                if (BETA <= ALPHA) break;
                            }
                        }
                    }
                    return batas;
                }
            }
        }
    }
}
