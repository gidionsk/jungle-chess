﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jungle_Chess {
    public class Petak : ICloneable<Petak> {
        //GAMBAR PETAK
        Image img;

        //BUTTON YANG DITAMPILKAN DI LAYAR
        public Button body;

        //X, Y
        public int x, y, size;

        //TIPE PETAK
        public int type;

        //OWNER PLAYER
        public string owner;

        //PION YANG ADA DI PETAK
        public Pion pion;

        public Petak Clone() {
            return new Petak(this.x, this.y, this.size, this.owner, this.type, this.pion);
        }

        public Petak(int x, int y, int size, string owner = "", int type= 0, Pion pion = null) {
            this.body = new Button();
            this.body.Name = x + "-" + y;
            this.body.SetBounds(x * size, y * size, size, size);
            this.x = x; this.y = y; this.size = size;
            this.owner = owner;
            this.type = type;
            this.pion = pion;
            
            /*
             TYPE
             -3 => DEN
             -2 => TRAP
             -1 => RIVER
              0 => LAHAN KOSONG
             */
        }

        public Petak(Petak clone) { //UNTUK CONSTRUCTOR CLONE
            this.owner = clone.owner;
            this.type = clone.type;
            this.pion = clone.pion;
        }

        public void setImage(Image param = null) {
            if (type == -3) { 
                if (this.owner == "p1") this.img = Image.FromFile("../../img/den1.png");
                else if (this.owner == "p2") this.img = Image.FromFile("../../img/den2.png");       
            } 
            else if (type == -2) this.img = Image.FromFile("../../img/trap.png");
            else if (type == -1) this.img = Image.FromFile("../../img/river.png");
            else if (type == 0) this.img = Image.FromFile("../../img/ground.png");
            
            if (param != null) this.body.BackgroundImage = param;
            else this.body.BackgroundImage = this.img;

            this.body.BackgroundImageLayout = ImageLayout.Stretch;
            this.body.BackColor = Color.Transparent;
            this.body.FlatStyle = FlatStyle.Flat;
        }
    }
}
