﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jungle_Chess {
    interface ICloneable<T> {
        T Clone();
    }
}
